﻿using UnityEngine;
using System.Collections;

public class StartClient : MonoBehaviour
{

	void OnEnable ()
	{
		Application.LoadLevel ("Arena");

		NetworkConnectionError error = Network.Connect (Settings.singleton.clientConnectionHostData);
		if (error != NetworkConnectionError.NoError) {
			HandleNetworkError (error.ToString ());
		}
	}
	
	void OnFailedToConnect (NetworkConnectionError error)
	{
		HandleNetworkError ("Could not connect to server: " + error.ToString ());
	}
	
	void OnConnectedToServer ()
	{
		// send name

		// play as client until disconnect
	}
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Network.Disconnect ();
		}
	}

	void OnDisconnectedFromServer (NetworkDisconnection info)
	{
		Debug.Log ("OnDisconnectedFromServer(" + info + ")");
		if (info == NetworkDisconnection.Disconnected) {
			GoToMenu ();
		} else {
			HandleNetworkError (info.ToString ());
		}
	}
	
	private void HandleNetworkError (string error)
	{
		Debug.LogError (error);
		
		MessageWindow.singleton.Show ("Network Error", error);
		
		GoToMenu ();
	}
	
	private void GoToMenu ()
	{
		Application.LoadLevel ("Menu");
		
		gameObject.AddComponent<BrowseServers> ();
		Destroy (this);
	}
}
