﻿using UnityEngine;
using System.Collections;

public class BrowseServers : MonoBehaviour
{
	/// Current scroll position of the server list.
	private Vector2 scrollPosition;
	
	void OnEnable ()
	{
		RefreshHostList ();
	}
	
	private void RefreshHostList ()
	{
		MasterServer.ClearHostList ();
		MasterServerConnection.singleton.RequestHostList (Settings.singleton.gameTypeName);
	}
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			gameObject.GetComponent<ClientOrServer> ().enabled = true;
			Destroy (this);
		}
	}

	void OnGUI ()
	{
		GUI.skin = Settings.singleton.skin;
		
		Texture refresh = Settings.singleton.refreshButton;
		
		GUILayout.BeginArea (new Rect (0, 0, Screen.width, Screen.height));
		GUILayout.BeginVertical ();

		GUILayout.Label ("");

		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label ("Available Games:");
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button (refresh, "", GUILayout.Width(Screen.width * 0.1f), GUILayout.Height(Screen.width * 0.1f / refresh.width * refresh.height))) {
		//if (GUILayout.Button ("Refresh", GUILayout.Width(Screen.width * 0.2f), GUILayout.Height(Screen.height * 0.1f))) {
			RefreshHostList ();
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		
		GUILayout.Label ("");

		scrollPosition = GUILayout.BeginScrollView (scrollPosition, false, true);
		HostData[] hosts = MasterServer.PollHostList ();
		switch (MasterServerConnection.singleton.state) {
		case MasterServerConnection.State.Unknown:
		case MasterServerConnection.State.Unreachable:
			gameObject.GetComponent<ClientOrServer> ().enabled = true;
			this.enabled = false;
			break;
		case MasterServerConnection.State.AwaitingHostList:
			GUILayout.Label ("Refreshing...");
			break;
		case MasterServerConnection.State.Idle:
			if (hosts.Length > 0) {
				foreach (HostData hostData in hosts) {
					if (GUILayout.Button (hostData.gameName, GUILayout.Height(Screen.height * 0.1f))) {
						Settings.singleton.clientConnectionHostData = hostData;
						gameObject.AddComponent<StartClient> ();
						Destroy (this);
					}
				}
			} else {
				GUILayout.BeginHorizontal ();
				GUILayout.FlexibleSpace ();
				GUILayout.Label ("There are currently no available games.");
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal ();
				GUILayout.FlexibleSpace ();
				GUILayout.Label ("You could create one yourself and wait for other players to join:");
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal ();
				GUILayout.FlexibleSpace ();
				if (GUILayout.Button ("Create New Game")) {
					gameObject.AddComponent<ConfigureServer> ();
					Destroy (this);
				}
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal ();
			}
			break;
		default:
			throw new UnityException ("FIXME BrowseServers");
		}
		GUILayout.EndScrollView ();
		
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
	}
}
