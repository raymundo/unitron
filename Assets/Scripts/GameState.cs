using UnityEngine;
using System.Collections;
using System;

public class GameState {
	
	public float baseBikeSpeed = 100.0f;
	public float baseAngularVelocity = Mathf.PI * 0.5f;
	public float matchEndDuration = 2.0f;
	public float baseBikeRadius = 15.0f;
	
	public static GameState singleton = new GameState();
	
	public enum State {
		LOBBY,
		MATCH,
		MATCH_END
	}
	
	public State state = State.MATCH;
	public float timeOfStateBegin = 0.0f;
	
	public Vector2 arenaSize = new Vector2(800, 800);
	
	public float BaseBikeSpeed() {
		switch (state) {
		case State.LOBBY:
			return 0.0f;
		case State.MATCH:
			return baseBikeSpeed;
		case State.MATCH_END:
			float timePassed = Time.realtimeSinceStartup - timeOfStateBegin;
			timePassed = Mathf.Clamp(timePassed, 0.0f, matchEndDuration);
			return baseBikeSpeed * (1.0f - (timePassed / matchEndDuration));
		default: throw new InvalidOperationException();
		}
	}
}
