using UnityEngine;
using System.Collections;

public class TouchController : LocalController
{
	
	void Start ()
	{
		// only work on mobile devices
		if (!Input.multiTouchEnabled) {
			Destroy (this);
		}
	}
	
	void Update ()
	{
		float newValue = 0;
		
		if (Input.touchCount == 2) {
			Vector2 delta = Input.touches [0].position - Input.touches [1].position;
			if (delta.x > 0) {
				delta *= -1;
			}
			delta.Normalize ();
			newValue = delta.y * 3;
			newValue = Mathf.Max (-1.0f, Mathf.Min (newValue, 1.0f));
		}
		
		SetSteeringValueToBikesOfLocalPlayer (newValue);
	}
}
