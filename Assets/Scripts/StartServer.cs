﻿using UnityEngine;
using System.Collections;

public class StartServer : MonoBehaviour
{

	void OnEnable ()
	{
		Application.LoadLevel ("Arena");
		
		bool useNat = true;
		switch (Settings.singleton.natPunchThrough) {
		case Settings.NAT_PunchThrough.On:
			useNat = true;
			break;
		case Settings.NAT_PunchThrough.Off:
			useNat = false;
			break;
		case Settings.NAT_PunchThrough.Auto:
			useNat = !Network.HavePublicAddress ();
			break;
		}
		NetworkConnectionError error = Network.InitializeServer (3, Settings.singleton.serverPort, useNat);
		if (error != NetworkConnectionError.NoError) {
			HandleNetworkError (error.ToString ());
		}
	}
	
	void OnServerInitialized ()
	{
		MasterServer.RegisterHost (Settings.singleton.gameTypeName, Settings.singleton.gameName);
		
		// play as server until disconnect
	}
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			MasterServer.UnregisterHost ();
			Network.Disconnect ();
		}
	}
	
	void OnDisconnectedFromServer (NetworkDisconnection info)
	{
		Debug.Log ("OnDisconnectedFromServer(" + info + ")");
		if (info == NetworkDisconnection.Disconnected) {
			GoToMenu ();
		} else {
			HandleNetworkError (info.ToString ());
		}
	}
	
	private void HandleNetworkError (string error)
	{
		Debug.LogError (error);
		
		MessageWindow.singleton.Show ("Network Error", error);
		
		GoToMenu ();
	}
	
	private void GoToMenu ()
	{
		Application.LoadLevel ("Menu");
		
		gameObject.AddComponent<ConfigureServer> ();
		Destroy (this);
	}
}
