using UnityEngine;
using System.Collections;

public class KeyboardController : LocalController
{

	void Update ()
	{
		float newValue = 0.0f;
		if (Input.GetKey (KeyCode.LeftArrow)) {
			newValue -= 1.0f;
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			newValue += 1.0f;
		}
		
		SetSteeringValueToBikesOfLocalPlayer (newValue);
	}
}
