using UnityEngine;
using System.Collections;

public class ConstantVelocityMovement : MonoBehaviour
{
	
	public Vector3 velocity;

	/// <summary>
	/// Moves this GameObject according to a given velocity vector.
	/// </summary>
	void FixedUpdate ()
	{
		transform.Translate (velocity * Time.fixedDeltaTime);
	}
}
