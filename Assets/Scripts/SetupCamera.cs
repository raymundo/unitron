using UnityEngine;
using System.Collections;

public class SetupCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.position = new Vector3 (
			GameState.singleton.arenaSize.x / 2,
	        GameState.singleton.arenaSize.y / 2, 
			transform.position.z
		);
		
		camera.orthographicSize = GameState.singleton.arenaSize.y / 2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
