using UnityEngine;
using System.Collections;

public class SetupGrid : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		transform.localScale = new Vector3 (
			GameState.singleton.arenaSize.x,
	        GameState.singleton.arenaSize.y, 
			1
		);

		transform.position = new Vector3 (
			GameState.singleton.arenaSize.x / 2,
	        GameState.singleton.arenaSize.y / 2, 
			transform.position.z
		);
//		transform.localScale.x = GameState.singleton.arenaSize.x;
//		transform.localScale.y = GameState.singleton.arenaSize.y;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
