using UnityEngine;
using System.Collections;
using System;

public class Settings : MonoBehaviour
{
	public enum NAT_PunchThrough
	{
		On,
		Auto,
		Off
	}
	
	public Texture refreshButton = null;
	public string gameTypeName = "Unitron";
	public string gameName = "A game without a name";
	public string nickName = "Nickname";
	public int serverPort = 55315;
	public NAT_PunchThrough natPunchThrough = NAT_PunchThrough.Auto;
	public HostData clientConnectionHostData = null;
	public float baseBikeSpeed = 100.0f;
	public float baseAngularVelocity = Mathf.PI * 0.5f;
	public float matchEndDuration = 2.0f;
	public float baseBikeRadius = 15.0f;
	public GUISkin skin = null;
	public static Settings singleton = null;
	
	void Awake ()
	{
		if (singleton == null) {
			singleton = this;
			
			nickName = SystemInfo.deviceName;
			;
		} else {
			throw new UnityException ("Singleton!");
		}
	}
}
