using UnityEngine;
using System.Collections;

public class LocalController : MonoBehaviour
{
	private float oldValue = 0;

	protected void SetSteeringValueToBikesOfLocalPlayer (float newValue)
	{
		if (oldValue == newValue) {
			return;
		}		

		// for all bikes
		foreach (GameObject go in GameObject.FindGameObjectsWithTag("LightBike")) {
			// use local steering value if bike is controlled by the local player
			if (go.GetComponent<Info> ().PlayerGUID == Network.player.guid) {
				go.GetComponent<LightBikeMovement> ().SteeringValue = newValue;
			}
		}
		
		oldValue = newValue;
	}
}
