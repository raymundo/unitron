﻿using UnityEngine;
using System.Collections;

public class MessageWindow : MonoBehaviour
{
	
	private string title = null;
	private string message = null;
	private Rect windowRect;
	public static MessageWindow singleton = null;
	
	void Awake ()
	{
		if (singleton == null) {
			singleton = this;
		} else {
			throw new UnityException ("Singleton!");
		}
	}
	
	public void Show (string title, string message)
	{
		this.title = title;
		this.message = message;
		this.windowRect = new Rect (Screen.width / 2, Screen.height / 2, 1, 1);
	}

	void OnGUI ()
	{
		if (title != null && message != null) {

			GUI.skin = Settings.singleton.skin;
		
			windowRect = new Rect (
				(Screen.width - windowRect.width) / 2,
				(Screen.height - windowRect.height) / 2,
				Mathf.Max (windowRect.width, windowRect.height),
				Mathf.Min (windowRect.width, windowRect.height));
			windowRect = GUILayout.Window (0, windowRect, DoMyWindow, title);
		}
	}

	void DoMyWindow (int windowID)
	{
		GUILayout.Label (message);
		if (GUILayout.Button ("OK")) {
			title = null;
			message = null;
		}
	}
}
