using UnityEngine;
using System.Collections;

public class TouchButtonsController : LocalController
{
	public float buttonWidthInch = 0.5f;
	public Texture left = null;
	public Texture right = null;
	private Rect rectLeft;
	private Rect rectRight;
	
	void Start ()
	{
		// only work on mobile devices
		if (!Input.multiTouchEnabled) {
			Destroy (this);
		}
		
		float dpi = (Screen.dpi == 0 ? 96 : Screen.dpi);

		float buttonWidthPixel = dpi * buttonWidthInch;
		float buttonHeightPixelLeft = buttonWidthPixel / left.width * left.height;
		rectLeft = new Rect (
			0, Screen.height - buttonHeightPixelLeft,
			buttonWidthPixel, buttonHeightPixelLeft);
		float buttonHeightPixelRight = buttonWidthPixel / right.width * right.height;
		rectRight = new Rect (
			Screen.width - buttonWidthPixel, Screen.height - buttonHeightPixelRight,
			buttonWidthPixel, buttonHeightPixelRight);
	}

	void OnGUI ()
	{
		GUI.DrawTexture (rectLeft, left);
		GUI.DrawTexture (rectRight, right);
		
		float newValue = 0;
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended) {
				continue;
			}
			
			Vector2 touchPosition = touch.position;
			touchPosition.y = Screen.height - touchPosition.y - 1;
			
			if (rectLeft.Contains (touchPosition)) {
				newValue -= 1;
				Debug.Log ("left");
			}
			if (rectRight.Contains (touchPosition)) {
				newValue += 1;
				Debug.Log ("right");
			}
		}

		SetSteeringValueToBikesOfLocalPlayer (newValue);
	}
}
