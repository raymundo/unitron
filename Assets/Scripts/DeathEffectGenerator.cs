﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NetworkView))]
public class DeathEffectGenerator : MonoBehaviour
{
	
	public GameObject prefab;
	public static DeathEffectGenerator instance;
	
	void Awake ()
	{
		if (instance != null) {
			throw new UnityException ("Singleton!");
		} else {
			instance = this;
		}
	}
	
	/// Play a death effect on all network nodes.
	public void Generate (Vector3 position, Vector3 velocity, string playerGUID)
	{
		LocalGenerate (position, velocity, playerGUID);
		if (Network.peerType == NetworkPeerType.Client || Network.peerType == NetworkPeerType.Server) {
			networkView.RPC ("LocalGenerate", RPCMode.Others, position, velocity, playerGUID);
		}
	}
	
	[RPC]
	private void LocalGenerate (Vector3 position, Vector3 velocity, string playerGUID)
	{
		GameObject deathEffect = (GameObject)Instantiate (prefab, position, Quaternion.identity);
		deathEffect.GetComponent<ConstantVelocityMovement> ().velocity = velocity;
		deathEffect.GetComponent<Info> ().PlayerGUID = playerGUID;
	}
}
