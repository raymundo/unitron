using UnityEngine;
using System.Collections;

public class AxisController : LocalController
{

	void Update ()
	{
		float newValue = Input.GetAxis("Horizontal");
		SetSteeringValueToBikesOfLocalPlayer (newValue);
	}
}
