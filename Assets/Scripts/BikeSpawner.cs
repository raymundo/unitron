using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NetworkView))]
public class BikeSpawner : MonoBehaviour
{
	public GameObject prefab;
	public static BikeSpawner instance;
	
	void Awake ()
	{
		if (instance != null) {
			throw new UnityException ("Singleton!");
		} else {
			instance = this;
		}
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			Vector3 position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			position.z = 0;
			Spawn (position, Quaternion.identity, Network.player.guid);
		}
	}

	/// Spawn a bike on all network nodes.
	public void Spawn (Vector3 position, Quaternion rotation, string playerGUID)
	{
		NetworkViewID viewID = Network.AllocateViewID ();
		LocalSpawn (position, rotation, viewID, playerGUID);
		if (Network.peerType == NetworkPeerType.Client || Network.peerType == NetworkPeerType.Server) {
			networkView.RPC ("LocalSpawn", RPCMode.Others, position, rotation, viewID, playerGUID);
		}
	}
	
	[RPC]
	private void LocalSpawn (Vector3 position, Quaternion rotation, NetworkViewID viewID, string playerGUID)
	{
		Debug.Log (playerGUID + " spawns a new bike with " + viewID + " at " + position);
		GameObject bike = (GameObject)Instantiate (prefab, position, rotation);
		bike.networkView.viewID = viewID;
		bike.name = "LightBike (GUID=" + playerGUID + ",viewID=" + bike.networkView.viewID + ")";
		bike.GetComponent<Info> ().PlayerGUID = playerGUID;
	}
}
