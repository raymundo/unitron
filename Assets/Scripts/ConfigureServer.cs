﻿using UnityEngine;
using System.Collections;

public class ConfigureServer : MonoBehaviour
{
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			gameObject.GetComponent<ClientOrServer> ().enabled = true;
			Destroy (this);
		}
	}

	void OnGUI ()
	{
		GUI.skin = Settings.singleton.skin;
		
		GUILayout.BeginArea (new Rect (0, 0, Screen.width, Screen.height));
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label("Your game's name (this will be displayed in the list of games):");
		Settings.singleton.gameName = GUILayout.TextField (Settings.singleton.gameName);
		GUILayout.FlexibleSpace ();
		GUILayout.Label("Your name:");
		Settings.singleton.nickName = GUILayout.TextField (Settings.singleton.nickName);
		GUILayout.FlexibleSpace ();
		GUILayout.Label("Use NAT punch-through: ");
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace ();
		Settings.singleton.natPunchThrough =
			(Settings.NAT_PunchThrough)GUILayout.SelectionGrid((int)Settings.singleton.natPunchThrough, Settings.NAT_PunchThrough.GetNames(typeof(Settings.NAT_PunchThrough)), 3);
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace ();
		if (GUILayout.Button("Start Game")) {
			gameObject.AddComponent<StartServer>();
			Destroy (this);
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
	}
}

