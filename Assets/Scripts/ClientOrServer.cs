﻿using UnityEngine;
using System.Collections;

public class ClientOrServer : MonoBehaviour
{
	public Texture logo = null;
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
	}

	void OnGUI ()
	{
		GUI.skin = Settings.singleton.skin;
		
		GUILayout.BeginArea (new Rect (0, 0, Screen.width, Screen.height));
		GUILayout.BeginVertical ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.Label (logo, GUILayout.Width(Screen.width * 0.7f), GUILayout.Height(Screen.width * 0.7f / logo.width * logo.height));
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		switch (MasterServerConnection.singleton.state) {
		case MasterServerConnection.State.AwaitingHostList:
			GUILayout.Box ("Connecting ...");
			break;
		case MasterServerConnection.State.Idle:
			GUILayout.FlexibleSpace ();
			if (GUILayout.Button ("Join Existing Game")) {
				gameObject.AddComponent<BrowseServers> ();
				this.enabled = false;
			}
			GUILayout.FlexibleSpace ();
			if (GUILayout.Button ("Create New Game")) {
				gameObject.AddComponent<ConfigureServer> ();
				this.enabled = false;
			}
			GUILayout.FlexibleSpace ();
			break;
		case MasterServerConnection.State.Unreachable:
			GUILayout.Box ("Network Error: " + MasterServerConnection.singleton.networkConnectionError.ToString () + "\n You need a connection to the internet to play.");
			break;
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.FlexibleSpace ();
		GUILayout.EndVertical ();
		GUILayout.EndArea ();
	}
}
