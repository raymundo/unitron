using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NetworkView))]
[RequireComponent(typeof(Info))]
public class LightBikeMovement : MonoBehaviour
{
	/// gets set by other scripts
	public float SteeringValue = 0.0f;
	private float lastSentSteeringValue = 0.0f;
	public Vector3 rotationAxis = new Vector3 (0, 0, -1);
	public GameObject trailPrefab;
	private GameObject lastTrailElement;
	private Vector3 lastColliderEnd;
	private int lineRendererVertexCount = 1;
	
	/// used to avoid multiple destructions if the bike collides with multiple colliders in the same frame
	private bool isDead = false;
	
	/// Contains information about the controlling player.
	private Info info;
	
	/// Used to visually cover the distance between the bike and the end of its trail.
	private LineRenderer lineRenderer;

	// Use this for initialization
	void Start ()
	{
		info = GetComponent<Info> ();
		lastColliderEnd = transform.position;
		
		lineRenderer = transform.GetComponentInChildren<LineRenderer> ();
		lineRenderer.SetVertexCount (lineRendererVertexCount);
		lineRenderer.SetPosition (lineRendererVertexCount - 1, transform.position);
	}
	
	void OnSerializeNetworkView (BitStream stream, NetworkMessageInfo info)
	{
		if (stream.isWriting) { // send
			// only send if SteeringValue has changed
			if (lastSentSteeringValue == SteeringValue) {
				return;
			}
			lastSentSteeringValue = SteeringValue;
		} else { // receive
		}
		stream.Serialize (ref SteeringValue);
		return;
		
		Vector3 position = transform.position;
		stream.Serialize (ref position);
		transform.position = position;
		
		Quaternion rotation = transform.rotation;
		stream.Serialize (ref rotation);
		transform.rotation = rotation;
	}
	
	/// <summary>
	/// Moves this GameObject according to the current steeringValue and speed (determined by gamestate).
	/// Spawns trail elements each time a certail distance has been travelled.
	/// </summary>
	void FixedUpdate ()
	{
		if (isDead) {
			return;
		}
		
		// movement
		if (SteeringValue != 0) {
		SteeringValue = Mathf.Clamp (SteeringValue, -1.0f, 1.0f); // already clamped on local input - remove this when also clamping value from network
		}
		float speed = GameState.singleton.BaseBikeSpeed ();
		float distance = speed * Time.fixedDeltaTime;
		float angularVelocity = GameState.singleton.baseAngularVelocity;
		float angle = angularVelocity * Time.fixedDeltaTime * SteeringValue;
		transform.RotateAround (rotationAxis, angle);
		transform.Translate (distance, 0.0f, 0.0f);
		
		// adjust LineRenderer's last segment to span from current position to position of last trail element
		lineRenderer.SetPosition (lineRendererVertexCount - 1, transform.position);
		
		// maybe spawn a new trail element
		if (Vector3.Distance (lastColliderEnd, transform.position) > GameState.singleton.baseBikeRadius * 0.5f) {
			advanceTrail ();
		}
	}
	
	/// <summary>
	/// Advances the trail by one element.
	/// </summary>
	private void advanceTrail ()
	{
		Vector3 position = (lastColliderEnd + transform.position) * 0.5f;
		Quaternion rotation = Quaternion.FromToRotation (new Vector3 (0, 1, 0), lastColliderEnd - transform.position);
		lastTrailElement = (GameObject)Instantiate (trailPrefab, position, rotation);
		lastTrailElement.name = gameObject.name + " trail " + (lineRendererVertexCount - 1);
		lastTrailElement.GetComponent<Info> ().PlayerGUID = GetComponent<Info> ().PlayerGUID;
		
		lastColliderEnd = transform.position;

		// add another segment to LineRenderer that's at the position of the just created trail element
		lineRendererVertexCount++;
		lineRenderer.SetVertexCount (lineRendererVertexCount);
		lineRenderer.SetPosition (lineRendererVertexCount - 2, position);
		lineRenderer.SetPosition (lineRendererVertexCount - 1, transform.position);
	}
	
	/// (Assume that) This bike just collided with a trail. Destroy it.
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject == lastTrailElement) {
			return;
		}
		
		// only die once
		if (isDead) {
			return;
		} else {
			isDead = true;
		}

		if (lastColliderEnd != transform.position) {
			advanceTrail ();
		}
		
		Debug.Log (gameObject.name + " collided with " + other.gameObject.name);

		// play a death effect that fits to this bike on all network nodes
		Vector3 velocity = (transform.position - lastTrailElement.transform.position).normalized * GameState.singleton.BaseBikeSpeed ();
		DeathEffectGenerator.instance.Generate(transform.position, velocity, info.PlayerGUID);
		
		NetworkViewID viewID = networkView.viewID;
		if (viewID.isMine) {
			// destroy this bike on all network nodes
			Debug.Log (gameObject.GetComponent<Info> ().PlayerGUID + ": destroying bike with " + viewID);
			Network.Destroy (viewID);
			Network.RemoveRPCs (viewID);
		}
	}
	
	void OnDestroy()
	{
		// disconnect trail from this bike (so it survives this bike's destruction)
		lineRenderer.gameObject.name = "Trail of dead " + gameObject.name;
		lineRenderer.gameObject.transform.parent = null;
	}
}
