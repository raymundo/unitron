using UnityEngine;
using System;

public class MasterServerConnection : MonoBehaviour
{
	public enum State
	{
		Unknown,
		Unreachable,
		AwaitingHostList,
		Idle
	}
	
	public string masterServerHost = "";
	public int masterServerPort = 0;
	public State state = State.Unknown;
	public NetworkConnectionError networkConnectionError = NetworkConnectionError.NoError;
	public static MasterServerConnection singleton = null;
	
	void Awake ()
	{
		if (singleton == null) {
			singleton = this;
		} else {
			throw new UnityException ("Singleton!");
		}
		
		Application.RegisterLogCallback (LogCallback);

		if (masterServerHost.Length > 0) {
			MasterServer.ipAddress = masterServerHost;
		}
		if (masterServerPort > 0) {
			MasterServer.port = masterServerPort;
		}
		
		RequestHostList ("Connection Test");
	}
	
	public void RequestHostList (string gameTypeName)
	{
		MasterServer.RequestHostList (gameTypeName);
		state = State.AwaitingHostList;
	}

	void OnFailedToConnectToMasterServer (NetworkConnectionError info)
	{
		Debug.Log ("Could not connect to master server: " + info);
		networkConnectionError = info;
		MessageWindow.singleton.Show ("Network Error", "Could not connect to master server: " + info);
	}

	void OnFailedToConnect (NetworkConnectionError info)
	{
		Debug.Log ("Could not connect: " + info);
		networkConnectionError = info;
		MessageWindow.singleton.Show ("Network Error", "Could not connect: " + info);
	}

	void OnMasterServerEvent (MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived) {
			state = State.Idle;
		}
	}
	
	private void LogCallback (string condition, string stackTrace, LogType type)
	{
		if (type == LogType.Error) {
			// Check if it is the NATPunchthroughFailed error 
			const string MessageBeginning = "Receiving NAT punchthrough attempt from target";
 
			if (condition.StartsWith (MessageBeginning, StringComparison.Ordinal)) {
				// Call the callback that Unity should be calling.
				OnFailedToConnect (NetworkConnectionError.NATPunchthroughFailed);
			}
		}
	}
}
