using UnityEngine;
using System.Collections;

/// <summary>
/// Stores the GUID of the controlling player for this GameObject. Also sets the main color of all renderers on this object depending on that id.
/// </summary>
public class Info : MonoBehaviour {
	
	private string m_playerGUID = "";
	public string PlayerGUID {
		get {
			return m_playerGUID;
		}
		set {
			m_playerGUID = value;
			Color color = ColorForPlayerGUID(m_playerGUID);
				
			MeshRenderer[] mr = GetComponentsInChildren<MeshRenderer>();
			if (mr != null) {
				foreach (MeshRenderer r in mr) {
					r.material.color = color;
				}
			}

			LineRenderer[] lr = GetComponentsInChildren<LineRenderer>();
			if (lr != null) {
				foreach (LineRenderer r in lr) {
					r.material.color = color;
				}
			}
			
			ParticleSystem ps = GetComponent<ParticleSystem>();
			if (ps != null) {
				ps.startColor = color;
			}
		}
	}
	
	public static Color ColorForPlayerGUID(string guid) {
		switch ((uint)(guid.GetHashCode()) % 6) {
		case 0: return Color.red;
		case 1: return Color.green;
		case 2: return new Color(0.2f, 0.4f, 1.0f);
		case 3: return Color.cyan;
		case 4: return Color.magenta;
		case 5: return Color.yellow;
		default: throw new UnityException("FIXME GameState");
		}
	}
}
